// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: 

    //$ mongoimport -d students -c grades < grades.json

    //RESPUESTA:
    //2020-09-28T14:01:58.023-0600	connected to: localhost
    //2020-09-28T14:01:59.035-0600	imported 800 documents


// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. 
//    Confirma que se importo correctamente la colección con los siguientes comandos 
//    en la terminal de mongo: 

    //use students; 
    db.grades.count() 

    //¿Cuántos registros arrojo el comando count?
    //RESPUESTA:
    //800


// 3) Encuentra todas las calificaciones del estudiante con el id numero 4
    db.grades.find({ student_id: 4 }, { score: 1, _id: 0 })
    //RESPUESTA:
    //{ "score" : 27.29006335059361 }
    //{ "score" : 5.244452510818443 }
    //{ "score" : 28.656451042441 }
    //{ "score" : 87.89071881934647 }  

// 4) ¿Cuántos registros hay de tipo exam?
    db.grades.count({ type: "exam" })
    //REPSUESTA:
    //200

// 5) ¿Cuántos registros hay de tipo homework?
    db.grades.count({ type: "homework" })
    //RESPUESTA:
    //400

// 6) ¿Cuántos registros hay de tipo quiz?
    db.grades.count({ type: "quiz" })
    //RESPUESTA:
    //200

// 7) Elimina todas las calificaciones del estudiante con el id numero 3
    db.grades.updateMany({ student_id: 3 }, { $set: { score: null } })
    //RESPUESTA:
    //{ "acknowledged" : true, "matchedCount" : 4, "modifiedCount" : 4 }
    //COMPROBACION:
    //db.grades.find({ student_id: 3 }, { score: 1, _id: 0 })
    //{ "score" : null }
    //{ "score" : null }
    //{ "score" : null }
    //{ "score" : null }

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?
    db.grades.find({ score: 75.29561445722392 }, { student_id: 1, _id: 0 })
    //RESPUESTA:
    //{ "student_id" : 9 }

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    db.grades.update({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { score: 100 } })
    //RESPUESTA:
    //WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
    //COMPROBACION:
    //db.grades.find({ "_id": ObjectId("50906d7fa3c412bb040eb591") })
    //{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }

// 10) A qué estudiante pertenece esta calificación.
    db.grades.find({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { _id: 0, student_id: 1})
    //RESPUESTA:
    //{ "student_id" : 6 }





